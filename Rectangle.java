public class Rectangle{
    int sideA, sideB;
    Point topLeft;

    public Rectangle(int sideA, int sideB, Point topLeft) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.topLeft = topLeft;
    }

    public int area() {
        return sideA * sideB;
    }

    public int perimeter() {
        return 2 * (sideA + sideB);
    }

    public Point[] corners() {
        Point topRight = new Point(topLeft.xCoord + sideA, topLeft.yCoord);
        Point bottomLeft = new Point(topLeft.xCoord, topLeft.yCoord - sideB);
        Point bottomRight = new Point(topLeft.xCoord + sideA, topLeft.yCoord - sideB);
        return new Point[]{topLeft,topRight, bottomLeft, bottomRight};
    }


}
