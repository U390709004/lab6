public class Circle {
    int radius;
    Point center;
    public Circle (int a, Point b) {
        this.radius = a;
        this.center = b;
    }

    public double area(){
        return (Math.PI *  (radius*radius));
    }

    public double perimeter(){
        return 2 * Math.PI *  radius;
    }

    public boolean intersect(Circle C1, Circle C2) {
        double l = Math.sqrt((C1.center.xCoord - C2.center.xCoord) *
                (C1.center.xCoord - C2.center.xCoord) + (C1.center.yCoord - C2.center.yCoord) *
                (C1.center.yCoord - C2.center.yCoord));
        if (l <= (C1.radius + C2.radius)){
            System.out.println("Given circles are intersecting");
            return true;

        }
        System.out.println("Given circles are not intersecting");

        return false;


    }
}
